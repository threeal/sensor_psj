#define RAIN_SENSOR_PIN A0

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  uint8_t message[2 + sizeof(int32_t)];
  message[0] = 255;
  message[1] = 2;

  int rain_value = 1024 - analogRead(RAIN_SENSOR_PIN);

  int32_t value = (int)((double)rain_value / 1024.0);
  memcpy(message + 2, &value, sizeof(value));

  Serial.write(message, sizeof(message));

  delay(2000);
}
