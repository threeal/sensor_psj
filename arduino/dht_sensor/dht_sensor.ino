#include "dht.h"

#define DHT_PIN A0

dht dht_sensor;

void setup()
{
  Serial.begin(9600);
  delay(500);
}

void loop()
{
  dht_sensor.read11(DHT_PIN);

  uint8_t message[2 + sizeof(int32_t)];
  message[0] = 255;
  message[1] = 0;

  int32_t value = (int32_t)dht_sensor.temperature;
  memcpy(message + 2, &value, sizeof(value));

  Serial.write(message, sizeof(message));

  message[1] = 1;

  value = (int32_t)dht_sensor.humidity;
  memcpy(message + 2, &value, sizeof(value));

  Serial.write(message, sizeof(message));

  delay(2000);
}
