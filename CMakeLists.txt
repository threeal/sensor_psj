cmake_minimum_required(VERSION 3.5)
project(sensor_jarkom)

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

find_package(Boost REQUIRED COMPONENTS thread)

include_directories(
  include
  ${Boost_INCLUDE_DIRS}
)

add_executable(
  server
    src/server.cpp
)

target_link_libraries(
  server
    ${Boost_LIBRARIES}
)

add_executable(
  client
    src/client.cpp
)

target_link_libraries(
  client
    ${Boost_LIBRARIES}
)

add_executable(
  client_dummy
    src/client_dummy.cpp
)

target_link_libraries(
  client_dummy
    ${Boost_LIBRARIES}
)