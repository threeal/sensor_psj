#include "boost/asio.hpp"
#include "boost/bind.hpp"
#include "boost/smart_ptr.hpp"
#include "boost/thread/thread.hpp"

#include <chrono>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <iostream>

using boost::asio::ip::tcp;

void session(boost::shared_ptr<tcp::socket> sock)
{
  try
  {
    std::ofstream of;

    while (true)
    {
      uint8_t data[2 + sizeof(int32_t)];

      boost::system::error_code error;
      size_t length = sock->read_some(boost::asio::buffer(data), error);

      if (error == boost::asio::error::eof)
      {
        std::cout << "terminated by client" << std::endl;
        return;
      }
      else if (error)
      {
        throw boost::system::system_error(error);
      }

      if (length < 6 || data[0] != (uint8_t)255)
        continue;

      int32_t value;
      memcpy(&value, data + 2, sizeof(value));

      switch (data[1])
      {
      case 0:
        std::cout << "receive temperature " << value << " celcius from client" << std::endl;
        of.open("temperature.log", std::ofstream::out | std::ofstream::app);
        of <<  value << std::endl;
        of.close();
        break;

      case 1:
        std::cout << "receive humidity " << value << " percent from client" << std::endl;
        of.open("humidity.log", std::ofstream::out | std::ofstream::app);
        of <<  value << std::endl;
        of.close();
        break;

      case 2:
        std::cout << "receive rain density " << value << " percent from client" << std::endl;
        of.open("rain_density.log", std::ofstream::out | std::ofstream::app);
        of <<  value << std::endl;
        of.close();
        break;

      default:
        std::cout << "unknown data received from client" << std::endl;
      }
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception in thread: " << e.what() << "\n";
  }
}

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: server <port>\n";
      return 1;
    }

    int port = atoi(argv[1]);

    boost::asio::io_service io_service;

    tcp::acceptor acc(io_service, tcp::endpoint(tcp::v4(), port));
    std::cout << "waiting for client" << std::endl;

    while (true)
    {
      boost::shared_ptr<tcp::socket> sock(new tcp::socket(io_service));
      acc.accept(*sock);

      std::cout << "connected to client" << std::endl;
      boost::thread t(boost::bind(session, sock));
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}