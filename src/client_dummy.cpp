#include "boost/asio.hpp"

#include <cstdlib>
#include <cstring>
#include <iostream>

using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: client_dummy <host> <port>\n";
      return 1;
    }

    char *host = argv[1];
    char *port = argv[2];

    boost::asio::io_service io_service;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(tcp::v4(), host, port);
    tcp::resolver::iterator iterator = resolver.resolve(query);

    tcp::socket sock(io_service);
    boost::asio::connect(sock, iterator);

    while (true)
    {
      uint8_t message[2 + sizeof(int32_t)];
      message[0] = 255;
      message[1] = 0;

      int32_t value = rand() % 1024;
      memcpy(message + 2, &value, sizeof(value));

      std::cout << "send dummy temperature " << value << std::endl;
      boost::asio::write(sock, boost::asio::buffer(message, sizeof(message)));
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}