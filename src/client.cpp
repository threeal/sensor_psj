#include "boost/asio/serial_port.hpp"
#include "boost/asio.hpp"

#include <iostream>

using boost::asio::ip::tcp;

int main(int argc, char **argv)
{
  try
  {
    if (argc != 5)
    {
      std::cerr << "Usage: client <host> <port> <serial_port> <baud_rate>\n";
      return 1;
    }

    char *host = argv[1];
    char *port = argv[2];
    char *serial_port = argv[3];
    int baud = atoi(argv[4]);

    boost::asio::io_service io_service;
    boost::asio::serial_port serial(io_service);

    serial.open(serial_port);
    serial.set_option(boost::asio::serial_port_base::baud_rate(baud));

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(tcp::v4(), host, port);
    tcp::resolver::iterator iterator = resolver.resolve(query);

    tcp::socket sock(io_service);
    boost::asio::connect(sock, iterator);

    while (true)
    {
      uint8_t message[2 + sizeof(int32_t)];
      boost::asio::read(serial, boost::asio::buffer(message, sizeof(message)));

      switch (message[1])
      {
      case 0:
        std::cout << "send temperature data from sensor" << std::endl;
        break;

      case 1:
        std::cout << "send humidity data from sensor" << std::endl;
        break;

      case 2:
        std::cout << "send rain density data from sensor" << std::endl;
        break;

      default:
        std::cout << "send unknown data from sensor" << std::endl;
      }

      boost::asio::write(sock, boost::asio::buffer(message, sizeof(message)));
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}